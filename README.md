# imgur-gallery

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Component test
```
cypress run-ct or cypress open-ct
```

### DNS Entry
```
currentlyt the server start with imgur-gallery.local, please add a DNS entry to it

if imgur blocks your requests again:
goto vue.config.js and change devServer.host change the value and update the DNS entry
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
