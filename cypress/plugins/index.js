const { startDevServer } = require('@cypress/webpack-dev-server')
const {devServer} = require('../../vue.config')
const WebpackConfig = require('../webpack.config')

module.exports = (on, config) => {
    on('dev-server:start', (options) => {
        return startDevServer({ options, webpackConfig: {...WebpackConfig, devServer} })
    })

    return config
};