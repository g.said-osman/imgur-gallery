import {shallowMount} from '@vue/test-utils'
import ImageViewer from '../../src/components/ImageViewer.vue'
import {ImgurImage} from '../../src/model/imgur-image'

describe('ImageViewer.vue', () => {
    let testImages = []
    before(() => {
        testImages = cy.fixture('images.json')
            .then(data => testImages = data.map(image => new ImgurImage(image)));
    })

    it('renders image.link when passed', () => {
        testImages.forEach(image => {
            const wrapper = shallowMount(ImageViewer, {
                propsData: {image}
            });
            const imgNode = wrapper.get(`#${image.id}`);
            expect(imgNode).not.null;

            expect(imgNode.attributes()['src'], `Test src' Attribute`).eq(image.link)
            expect(imgNode.attributes()['alt'], `Test src' Attribute`).eq(image.title)
        })
    })
})