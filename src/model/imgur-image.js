export const CONTENT_TYPE_IMAGE = ['image/jpeg', 'image/png', 'image/gif']

export class ImgurImage {
    constructor({
                    description,
                    downs,
                    favorite_count,
                    height,
                    id,
                    in_most_viral,
                    link,
                    points,
                    score,
                    title,
                    type,
                    ups,
                    vote,
                    width
                }) {
        this.description = description
        this.downs = downs
        this.favorite_count = favorite_count
        this.height = height
        this.id = id
        this.in_most_viral = in_most_viral
        this.link = link
        this.points = points
        this.score = score
        this.title = title
        this.type = type
        this.ups = ups
        this.vote = vote
        this.width = width
    }
}