import Axios from 'axios'
import {CONTENT_TYPE_IMAGE, ImgurImage} from '../model/imgur-image'

export const SORT_VIRAL = 'viral'
export const SORT_TOP = 'top'
export const SORT_TIME = 'time'
export const SORT_RISING = 'rising'

export const SORTS = [SORT_VIRAL, SORT_TOP, SORT_TIME, SORT_RISING]

export const WINDOW_DAY = 'day'
export const WINDOW_WEEK = 'week'
export const WINDOW_MONTH = 'month'
export const WINDOW_YEAR = 'year'
export const WINDOW_ALL = 'all'

export const WINDOWS = [WINDOW_DAY, WINDOW_WEEK, WINDOW_MONTH, WINDOW_YEAR, WINDOW_ALL]

export const SECTION_HOT = 'hot'
export const SECTION_TOP = 'top'
export const SECTION_USER = 'user'

export const SECTIONS = [SECTION_HOT, SECTION_TOP, SECTION_USER]

const ImgurClient = Axios.create({
    baseURL: `${process.env.VUE_APP_IMGUR_API_BASE_URL}/${process.env.VUE_APP_IMGUR_API_VERSION}`,
    headers: {
        Authorization: `Client-ID ${process.env.VUE_APP_IMGUR_API_CLIENT_ID}`,
    }
})

const ImgurService = {
    /**
     * @param {String} section
     * @param {String} sort
     * @param {Number} page
     * @param {String} window
     * @param {Boolean} showViral
     * @param {CancelTokenSource} cancelToken
     * @returns {Promise<Array<ImgurImage>>}
     */
    loadImages ({section, sort, page = 500, window, showViral, cancelToken}) {
        return ImgurClient
            .get(`/gallery/${section}/${sort}/${window}/${page}?showViral=${showViral}`,
                {cancelToken: cancelToken ? cancelToken.token : null})
            // imgur use a custom property "success" to indicate a successful request/response instead of only using HTTP-Status for that
            // Maybe this is inconsitent, because of redundant usage of success indication - it's not documented
            .then(response => (!response.data.success && throw new Error(response.data)) || response)
            .then(response => response.data.data
                    .flatMap(gallery => gallery.images && gallery.images
                        .map(image => new ImgurImage(image)))
                    .filter(image => image && CONTENT_TYPE_IMAGE.includes(image.type)))
    }
}

const ImgurServicePlugin = {
    install: (Vue) => Vue.prototype.$ImgurService = ImgurService
}

export default ImgurServicePlugin