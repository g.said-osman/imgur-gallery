import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'

import App from './App.vue'
import store from './store'

import ImgurServicePlugin from './service/imgur-service'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(ImgurServicePlugin)

Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
