import {SECTIONS, SORTS, WINDOWS} from '../../service/imgur-service'
import Axios from "axios";

export const ACTION_SET_IMAGES = 'setImages'
export const ACTION_ADD_IMAGES = 'addImages'

const MUTATION_SET_IMAGES = 'MUTATION_SET_IMAGES'
const MUTATION_ADD_IMAGES = 'MUTATION_ADD_IMAGES'
const MUTATION_SET_GRID_OPTION_WIDTH = 'MUTATION_SET_GRID_OPTION_WIDTH'

const MUTATION_SET_SECTION = 'MUTATION_SET_SECTION'
const MUTATION_SET_SORT = 'MUTATION_SET_SORT'
const MUTATION_SET_WINDOW = 'MUTATION_SET_WINDOW'
const MUTATION_SET_SHOW_VIRAL = 'MUTATION_SET_SHOW_VIRAL'

const MUTATION_SET_INCREMENT_PAGE = 'MUTATION_SET_INCREMENT_PAGE'
const MUTATION_RESET_PAGE = 'MUTATION_RESET_PAGE'
const MUTATION_ALL_PAGES_LOADED = 'MUTATION_ALL_PAGES_LOADED'

const MUTATION_SET_CANCEL_TOKEN = 'MUTATION_SET_CANCEL_TOKEN'
const MUTATION_UNSET_CANCEL_TOKEN = 'MUTATION_UNSET_CANCEL_TOKEN'

const state = {
    images: null,
    requestOptions: {
        section: SECTIONS[0],
        sort: SORTS[0],
        window: WINDOWS[0],
        page: 1,
        showViral: true,
    },
    allPagesLoaded: false,
    cancelToken: null
}

const getters = {
    imagesToShow: state => state.images,
    loading: state => !!state.cancelToken
}

const loadImages = ({mutationType, commit, vm}) => {
    state.cancelToken && state.cancelToken.cancel()
    commit(MUTATION_SET_CANCEL_TOKEN, Axios.CancelToken.source())

    return vm.$ImgurService.loadImages({
        ...state.requestOptions,
        cancenToken: state.cancenToken
    })
    .then(images => (images.length > 0 && commit(mutationType, images)) || commit(MUTATION_ALL_PAGES_LOADED))
    .finally(() => {
        commit(MUTATION_UNSET_CANCEL_TOKEN)
    })
}

const actions = {
    [ACTION_SET_IMAGES]({commit, state}) {
        commit(MUTATION_RESET_PAGE)
        return loadImages({mutationType: MUTATION_SET_IMAGES, commit, state, vm: this._vm})
    },
    [ACTION_ADD_IMAGES]({commit, state}) {
        return loadImages({mutationType: MUTATION_ADD_IMAGES, commit, state, vm: this._vm})
    },
    setGridOptionWidth({commit}, gridOptionWidth) {
        commit(MUTATION_SET_GRID_OPTION_WIDTH, gridOptionWidth)
    },
    setSection({commit, dispatch}, section) {
        commit(MUTATION_SET_SECTION, section)
        return dispatch(ACTION_SET_IMAGES)
    },
    setSort({commit, dispatch}, sort) {
        commit(MUTATION_SET_SORT, sort)
        return dispatch(ACTION_SET_IMAGES)
    },
    setWindow({commit, dispatch}, window) {
        commit(MUTATION_SET_WINDOW, window)
        return dispatch(ACTION_SET_IMAGES)
    },
    setViral({commit, dispatch}, showViral) {
        commit(MUTATION_SET_SHOW_VIRAL, showViral)
        return dispatch(ACTION_SET_IMAGES)
    },
    incrementPage({commit, dispatch, state}) {
        if (!state.cancelToken && !state.allPagesLoaded) {
            commit(MUTATION_SET_INCREMENT_PAGE)
            return dispatch(ACTION_ADD_IMAGES)
        }
    }
}

const mutations = {
    [MUTATION_SET_IMAGES](state, images) {
        state.images = images
    },
    [MUTATION_ADD_IMAGES](state, images) {
        state.images = state.images.concat(images)
    },
    [MUTATION_SET_GRID_OPTION_WIDTH](state, gridOptionWidth) {
        state.gridOptionWidth = gridOptionWidth
    },
    [MUTATION_SET_SECTION](state, section) {
        state.requestOptions.section = section
    },
    [MUTATION_SET_SORT](state, sort) {
        state.requestOptions.sort = sort
    },
    [MUTATION_SET_WINDOW](state, window) {
        state.requestOptions.window = window
    },
    [MUTATION_SET_CANCEL_TOKEN](state, cancelToken) {
        state.cancelToken = cancelToken
    },
    [MUTATION_UNSET_CANCEL_TOKEN](state) {
        state.cancelToken = null
    },
    [MUTATION_SET_INCREMENT_PAGE](state) {
        state.requestOptions.page++
    },
    [MUTATION_RESET_PAGE](state) {
        state.requestOptions.page = 1
        state.allPagesloaded = false
    },
    [MUTATION_ALL_PAGES_LOADED](state) {
        state.allPagesloaded = true
    },
    [MUTATION_SET_SHOW_VIRAL](state, showViral) {
        state.requestOptions.showViral = showViral
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}